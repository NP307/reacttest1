import React, {Component} from 'react';
import logo from './Images/Erleada-60mg_R_4CP.png';
import leftded from './Images/ERL0077_HR_F.png';
import rightded from './Images/ERL0078_HR_F.png';
import './App.css'
import './fonts.css';

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            firstActive: 0,
            secondActive: 0,
        }
    }

    /*componentDidMount() {
        this.openTexts();

    }

    openTexts() {
        if (this.state.firstActive === 1 && this.state.secondActive === 0) {
            document.querySelector('.mainBlock').style.height='0%';
            document.querySelector('.firstText').style.height='calc(90% - 4px)';
            document.querySelector('.secondText').style.height='calc(10% - 4px)';
        } else if (this.state.firstActive === 0 && this.state.secondActive === 1) {
            document.querySelector('.mainBlock').style.height='0%';
            document.querySelector('.firstText').style.height='calc(10% - 4px)';
            document.querySelector('.secondText').style.height='calc(90% - 4px)';
        } else if (this.state.firstActive === 1 && this.state.secondActive === 1) {
            document.querySelector('.mainBlock').style.height='0%';
            document.querySelector('.firstText').style.height='calc(50% - 4px)';
            document.querySelector('.secondText').style.height='calc(50% - 4px)';
        } else if (this.state.firstActive === 0 && this.state.secondActive === 0) {
            document.querySelector('.mainBlock').style.height='80%';
            document.querySelector('.firstText').style.height='calc(10% - 4px)';
            document.querySelector('.secondText').style.height='calc(10% - 4px)';
        }
    }*/

    render() {
        const { firstActive, secondActive } = this.state;
        let first = firstActive === 1 && secondActive === 0 ? 'calc(90% - 4px)' : firstActive === 0 && secondActive === 1 ? 'calc(10% - 4px)' : firstActive === 1 && secondActive === 1 ? 'calc(50% - 4px)' : 'calc(10% - 4px)';
        let second = firstActive === 1 && secondActive === 0 ? 'calc(10% - 4px)' : firstActive === 0 && secondActive === 1 ? 'calc(90% - 4px)' : firstActive === 1 && secondActive === 1 ? 'calc(50% - 4px)' : 'calc(10% - 4px)';
        let main = firstActive === 1 && secondActive === 0 ? '0%' : firstActive === 0 && secondActive === 1 ? '0%' : firstActive === 1 && secondActive === 1 ? '0%' : '80%';
        return (
            <div style={{position: 'relative', width: 1310, height: 554, margin: 'auto', border: '50px solid black'}}>
                <span style={{color: 'white', position: 'absolute', top: -35, fontFamily: 'serif', fontWeight: 'bold'}}>CancerTherapy Advisor</span>
                <span style={{color: 'white', position: 'absolute', top: -35, fontFamily: 'serif', fontWeight: 'bold', right: 0}}>Patient Journey ~</span>
                <div className='mainBlock' style={{overflow: 'hidden', height: main, transition: '1000ms'}}>
                    <div style={{overflow: 'hidden', position: 'relative', width: '100%', textAlign: 'center', marginTop: 15}}>
                        <img src={logo} alt="logo" width={110} height={30} style={{position: 'absolute', top: -3, left: 16}}/>
                        <span className='asd'
                              style={{fontFamily: 'Khand',
                                  fontSize: 39,
                                  fontStyle: 'normal',
                                  fontWeight: 700,
                                  lineHeight: '48px',
                                  letterSpacing: 0,
                                  color: '#3E7CB1',
                              }}>Start treatment early for your patients with mCSPC</span>
                    </div>
                    <div style={{display: 'flex', overflow: 'hidden'}}>
                        <div style={{width: '50%', position: 'relative'}}>
                            <img src={leftded} height='100%' alt=""/>
                            <div style={{position: 'absolute', right: '10%', top: '30%', display: 'flex', flexDirection: 'column', alignItems: 'center', width: 200, textAlign: 'center'}}>
                        <span style={{
                            fontFamily: 'Khand',
                            fontSize: 39,
                            fontStyle: 'normal',
                            fontWeight: 700,
                            lineHeight: '48px',
                            letterSpacing: 0,
                            marginBottom: 10,
                        }}>ANTHONY*</span>
                                <span style={{
                                    fontFamily: 'Roboto',
                                    fontSize: 16,
                                    fontStyle: 'normal',
                                    fontWeight: 400,
                                    lineHeight: '22px',
                                    letterSpacing: 0,
                                    marginBottom: 10,
                                }}>68-year-old male newly diagnosed with mCSPC</span>
                                <button style={{
                                    backgroundColor: '#3E7CB1',
                                    border: 0,
                                    color: 'white',
                                    padding: 10,
                                    fontFamily: 'Open Sans',
                                    fontSize: 12,
                                    fontStyle: 'normal',
                                    fontWeight: 600,
                                    lineHeight: '16.34px',
                                    marginBottom: 10,
                                }}>READ STORY</button>
                                <span style={{
                                    fontFamily: 'Open Sans',
                                    fontSize: 12,
                                    fontStyle: 'normal',
                                    fontWeight: 400,
                                    lineHeight: '24px',
                                    color: '#575757',
                                    marginBottom: 10,
                                }}>*Not an actual patient.</span>
                            </div>
                        </div>
                        <div style={{width: '50%', position: 'relative'}}>
                            <div style={{position: 'absolute', left: '10%', top: '30%', display: 'flex', flexDirection: 'column', alignItems: 'center', width: 200, textAlign: 'center'}}>
                        <span style={{
                            fontFamily: 'Khand',
                            fontSize: 39,
                            fontStyle: 'normal',
                            fontWeight: 700,
                            lineHeight: '48px',
                            letterSpacing: 0,
                            marginBottom: 10,
                        }}>TOM*</span>
                                <span style={{
                                    fontFamily: 'Roboto',
                                    fontSize: 16,
                                    fontStyle: 'normal',
                                    fontWeight: 400,
                                    lineHeight: '22px',
                                    letterSpacing: 0,
                                    marginBottom: 10,
                                }}>76-year-old male with mCSPC and a comorbidity</span>
                                <button style={{
                                    backgroundColor: '#3E7CB1',
                                    border: 0,
                                    color: 'white',
                                    padding: 10,
                                    fontFamily: 'Open Sans',
                                    fontSize: 12,
                                    fontStyle: 'normal',
                                    fontWeight: 600,
                                    lineHeight: '16.34px',
                                    marginBottom: 10,
                                }}>READ STORY</button>
                                <span style={{
                                    fontFamily: 'Open Sans',
                                    fontSize: 12,
                                    fontStyle: 'normal',
                                    fontWeight: 400,
                                    lineHeight: '24px',
                                    color: '#575757',
                                    marginBottom: 10,
                                }}>*Not an actual patient.</span>
                            </div>
                            <img src={rightded} height='100%' alt=""/>
                        </div>
                    </div>
                </div>
                <div onClick={() => {
                    if (firstActive === 1) {
                        this.setState({firstActive: 0});
                    }
                    else {
                        this.setState({firstActive: 1});
                    }
                }} className='firstText' style={{height: first, transition: '1000ms', overflow: 'auto', cursor: 'pointer', borderTop: '2px solid #bbb', borderBottom: '2px solid #bbb'}}>
                    <div style={{margin: 10}}>
                <span style={{
                    fontFamily: 'Roboto',
                    fontSize: 30,
                    fontStyle: 'normal',
                    fontWeight: 400,
                    letterSpacing: 0,
                    marginBottom: 10,
                }}>
                    Ischemic Cardiovascular Events — In  randomized study (SPARTAN) of patients
                </span>
                    </div>
                    <div>
                <span style={{
                    fontFamily: 'Roboto',
                    fontSize: 30,
                    fontStyle: 'normal',
                    fontWeight: 400,
                    letterSpacing: 0,
                    marginBottom: 10,
                }}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid assumenda dolores exercitationem expedita facere harum non sed sint sit, ullam vitae voluptatibus? A alias animi at, cum, dolorem eligendi esse incidunt laboriosam laudantium mollitia nesciunt non nostrum officiis soluta totam ut voluptas. A adipisci aliquid corporis culpa, illo quibusdam reprehenderit sit. Ad animi molestiae sequi.
                </span>
                    </div>
                </div>
                <div onClick={() => {
                    if (secondActive === 1) {
                        this.setState({secondActive: 0});
                    }
                    else {
                        this.setState({secondActive: 1});
                    }
                }} className='secondText' style={{height: second, transition: '1000ms', overflow: 'auto', cursor: 'pointer', borderTop: '2px solid #bbb', borderBottom: '2px solid #bbb'}}>
                    <div style={{margin: 10}}>
                <span style={{
                    fontFamily: 'Roboto',
                    fontSize: 30,
                    fontStyle: 'normal',
                    fontWeight: 400,
                    letterSpacing: 0,
                    marginBottom: 10,
                }}>
                    Ischemic Cardiovascular Events — In  randomized study (SPARTAN) of patients
                </span>
                    </div>
                    <div>
                <span style={{
                    fontFamily: 'Roboto',
                    fontSize: 30,
                    fontStyle: 'normal',
                    fontWeight: 400,
                    letterSpacing: 0,
                    marginBottom: 10,
                }}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid assumenda dolores exercitationem expedita facere harum non sed sint sit, ullam vitae voluptatibus? A alias animi at, cum, dolorem eligendi esse incidunt laboriosam laudantium mollitia nesciunt non nostrum officiis soluta totam ut voluptas. A adipisci aliquid corporis culpa, illo quibusdam reprehenderit sit. Ad animi molestiae sequi.
                </span>
                    </div>
                </div>
                <span style={{color: 'white'}}>{this.state.firstActive.toString()}</span>
                <span style={{color: 'white'}}>{this.state.secondActive.toString()}</span>
            </div>
        )
    }
}

export default App;
